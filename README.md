## 前言
基于JQuery+JQueryUI实现的一款树形拖拽插件，效果图如下

![](https://user-gold-cdn.xitu.io/2020/4/14/171781bad5f3d857?w=1237&h=602&f=png&s=26027)

## 项目目录
* plugins 插件目录
    * jquery 存放jquery相关文件
    * jqueryui 存放jqueryui相关文件
    * treeDrag 树形拖拽插件实现相关文件
* src
    * assets 静态资源存放目录
        * css 插件内用到的css文件
        * js 插件内用到的js文件
    * config 树形结构渲染需要的文件
        * treeDragData.json 渲染树形结构的json数据(该功能尚未实现)
    * index.html 树形拖拽插件演示DEMO文件